var mysql = require('mysql');

var config = {
  server: "localhost",
  user: "root",
  password: "",
  database: "stats"
};


let today = new Date();
let dd = String(today.getDate()).padStart(2, '0');
let mm = String(today.getMonth() + 1).padStart(2, '0');
let yyyy = today.getFullYear();

today = dd + '/' + mm + '/' + yyyy;


let con = mysql.createConnection(config);
let query = "INSERT INTO visits (numVisits, date) VALUES (NULL, "+ today +")";

con.query(query, (error, results) => {
  if (error) {
    return console.error(error.message);
  }
  console.log(results);
});

con.end(); 