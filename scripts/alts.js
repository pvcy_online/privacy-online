let fs = require('fs');

function select (d, s/* , lang */) {
    /* if (lang == 'localhost') {
        lang = 'en';
    } */

    let data = fs.readFileSync("./en/alts/s1.json", 'utf8');
    
    let altsToSend = [];
    let alts = JSON.parse(data);

    for (let index in alts) {
        if (alts[index].device.includes(d) && alts[index].scale.includes(Number(s))) {
            altsToSend.push(alts[index]);
        }
    }

    return altsToSend;
}


module.exports = {
    select
}