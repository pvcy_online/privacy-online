# Privacy-online.info
This is the official repository of [privacy-online.info](https://privacy-online.info), a site made to raise awareness about online privacy and suggest privacy-friendly alternatives.

## Availability
[![Website](https://img.shields.io/website?url=https%3A%2F%2Fprivacy-online.info)](https://privacy-online.info/)

This website is now available in English on [privacy-online.info](https://privacy-online.info)
and in French on [fr.privacy-online.info](https://fr.privacy-online.info) !

## General
### Hosting
This website is hosted in Germany on servers operated by [Linode](https://linode.com). It is therefore under GDPR laws from the EU and German data laws.

### Language
The website is written in HTML/CSS and JavaScript. Server side scripting is done with Node.js.

### Privacy
There is a very light analytics program that is run when visiting the Home page. It is home-made and only collects the date. No private information is ever manipulated.
No cookies are used. No third-party requests are made. Everything is served from our servers.

### License
This project is available under the Eclipse Public License.

You can           | You cannot    | You must
---               |---            | ---
Commercial Use    | Use trademark | Include license
Modify            | Hold Liable   | Disclose Source
Distribute        |               | Include Copyright
Sublicense        |               | Compensate for Damages
Use Patent Claims |               | Include Install Install Instructions
Use Privately     |               | Include Original

Please refer to LICENSE file for more detailed information.

### Fonts
The fonts for the main title and the page titles are Zilla Slabs, Mozilla's font.
All the rest is Glacial Indifference, downloaded on [Open Font Library](https://fontlibrary.org/en/font/glacial-indifference).

### Images
All images are taken from [Unsplash](https://unsplash.com) or [Pixabay](https://pixabay.com),
icons are from [Iconmonstr](https://iconmonstr.com),
and the logo is from [Freepik](https://www.freepik.com).

### Install Instructions
First, you need to clone this repo :
```bash
git clone https://gitlab.com/pvcy_online/privacy-online.git
```
Then you enter the directory with `cd privacy-online` and you can start the website with 
```bash
node server.js
```

If you see `Node server is running on port 8080...`, you can open [localhost:8080](http://localhost:8080) in your browser.
Check that the port matches the one printed in you terminal.
