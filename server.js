#!/usr/bin/env node

let express = require('express');
let app = express();
let path = require('path');
let fs = require('fs');
let db = require('./dbconfig.js');
let mysql = require('mysql2');
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.disable('x-powered-by');



function db_connect(dbname) {
    db.database = dbname;
    return mysql.createConnection(db);
}

function language(host) {
    let lang = host.split('.')[0];
    if (lang === 'privacy-online' || lang === 'bottleneck' || lang === 'localhost') {
        lang = 'en';
    } else if (lang === '192') {
        lang = 'fr';
    }
    return lang;
}





// GET forbidden
// Format : /dbconfig.js || /.git*
app.get(/^\/(dbconfig.js|.git*|localhost.sql)/, (req, res) => {
    res.status(403).sendFile(path.join(__dirname, "errors", "forbidden.html"));
});


// POST suggestions
// Format : /suggestions/submit
app.post('/suggestions/submit', (req, res) => {
    let original = req.body.original;
    let name = req.body.name;
    let website = req.body.website;
    let category = req.body.category;
    let why = req.body.why;

    let database = db_connect('alts');

    let q = `INSERT INTO suggestions (original, name, website, category, why) VALUES (?, ?, ?, ?, ?);`;
    let placeholders = [original, name, website, category, why];

    database.query(q, placeholders, (error) => {
        if (error) {
            res.status(500).send(`${q} ${error.message}`);
        } else {
            res.send("ok");
        }
    });
    database.end();
});


// GET and sort alternatives
// Format : /alts/{device}-{scale}
app.get('/alts/:device-:scale', (req, res) => {
    let lang = language(req.hostname);

    let device = req.params.device;
    let scale = req.params.scale;
    let database = db_connect('alts');

    let q = "SELECT * FROM `common` JOIN ?? ON `common`.id = ??.id WHERE ?? = 1 AND ?? = 1;";
    let placeholders = [lang, lang, device, String.fromCharCode(96 + Number(scale))];

    database.query(q, placeholders, (error, results) => {
        if (error) {
            res.status(500).send(q + " " + error.message);
        } else {
            console.log(mysql.format(q, placeholders));
            res.send(results);
        }
    });
    database.end();
});


// GET run database query
// Format : /db/stats
app.post('/db/stats', (req, res) => {
    // Only save statistics if useragent doesn't contain "Dev" to exclude testers
    if (/Dev/.test(req.get('user-agent'))) {
        res.send("skipped");
    } else {
        let database = db_connect('stats');

        let currentDate = new Date();
        let cDay = currentDate.getDate();
        let cMonth = currentDate.getMonth() + 1;
        let cYear = currentDate.getFullYear();
        let date = cDay + "/" + cMonth + "/" + cYear;

        let q = `INSERT INTO visits (date) VALUES (?)`;
        let placeholders = [date];

        database.query(q, placeholders, (error, results) => {
            if (error) {
                res.send(error.message);
            } else {
                res.send("saved");
            }
        });
        database.end();
    }
});


// GET articles
// Format : /articles/[article name]
app.get('/articles/:name', (req, res) => {
    let name = req.params.name;
    let lang = language(req.hostname);


    let file = path.join(__dirname, lang, "reads", `${name}.html`);
    if (fs.existsSync(file)) {
        res.sendFile(file);
    } else {
        res.status(404).sendFile(path.join(__dirname, "errors", "notfound.html"));
    }
});


app.get("*", (req, res) => {
    // GET static files (except html)
    // Format : [any uri].[any file extension]
    if (req.url.includes('.')) {
        let file = path.join(__dirname, req.url);
        if (fs.existsSync(file)) {
            res.sendFile(file);
        } else {
            res.status(404).sendFile(path.join(__dirname, "errors", "notfound.html"));
        }
    }

    // GET html pages
    // Format : https://[language.]privacy-online.info/[any uri without file extension]
    else {
        let lang = language(req.hostname);

        if (req.url === "/") {
            res.sendFile(path.join(__dirname, lang, "index.html"));
        } else {
            let file = path.join(__dirname, lang, req.url.split('?')[0] + ".html");
            if (fs.existsSync(file)) {
                res.sendFile(file);
            } else {
                res.status(404).sendFile(path.join(__dirname, "errors", "notfound.html"));
            }
        }
    }
});


let port = process.env.PORT || 8080;
app.listen(port, (err) => {
    if (err) throw err;
    console.log(`Node server is running on port ${port}...`);
});